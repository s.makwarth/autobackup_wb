# README AutoBackup_WB

## Description
This script is used for backing up local projectfolders to a certain 
folder on the commondrive. The output is zipped with a timestamp
Older backups are not removed in the process.	

## How to use
Change the input and output path and run the batch file.

## Steps
The script follows these steps:
### - Copy all within the input folder to the output folder
### - Zipping the copyed folder in the same diretory
### - Removing the copyed folder