@ECHO OFF
:: ################################################################
:: This script is used for backing up local projectfolders to a certain 
:: folder on the commondrive. The output is zipped with a timestamp
:: Older backups are not removed in the process.
::
:: Author: Simon Makwarth, MST
:: simon dot makwarth at gmail dot com
:: ################################################################

set DateStamp=%date:~0,2%%date:~3,2%%date:~6,6%
set backupcmd=xcopy /s /c /d /e /h /i /r /y
set SourceDir=E:\
set DestinationDir=F:\GKO\Arkiv\Backup\SIMAK\

set ProjectName=Isenvad
echo ################ Backup Initiated! ################
echo.
echo.
(for %%a in (%ProjectName%) do ( 
    echo ################ Backing up %%a... ################
    echo Start Copy "%SourceDir%%%a" to "%DestinationDir%%%a\%%a_%DateStamp%" 
    %backupcmd% "%SourceDir%%%a" "%DestinationDir%%%a\%%a_%DateStamp%" >nul
    echo Copy done - Start Zip "%DestinationDir%%%a\%%a_%DateStamp%.zip" from "%DestinationDir%%%a\%%a_%DateStamp%"
    "C:\Program Files\7-Zip\7z.exe" a -tzip -r "%DestinationDir%%%a\%%a_%DateStamp%.zip" "%DestinationDir%%%a\%%a_%DateStamp%"
    echo Zip done - Removing folder "%DestinationDir%%%a\%%a_%DateStamp%"
    RMDIR "%DestinationDir%%%a\%%a_%DateStamp%"  /S /Q 
    echo ################ Backing up %%a Complete... ################ 
    echo.
    echo.
))
echo ################ Backup Complete! ################
pause
